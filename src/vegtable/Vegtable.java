/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author jayvy
 */
public abstract class Vegtable {
    String color;
    double size;
    
    public  Vegtable(String color, double size){
    this.size=size;
    this.color=color;
    }
    
    public double getSize(){
        return size;
    }
    
    public String getColor(){
return color;
    }
    
    public abstract boolean isRipe();
        
        
    
    
}
