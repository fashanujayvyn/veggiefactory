/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author jayvy
 */
import java.util.*;

public class VegtableSimulation {
    public static void main(String args[]){
        
        ArrayList<Vegtable> veg1 = new ArrayList<Vegtable>();
        VegetableFactory factory = VegetableFactory.getInstance();
        Vegtable car1 = factory.getVegetable(VegetableType.CARROT, "Red", 2);
        Vegtable car2 = factory.getVegetable(VegetableType.CARROT, "Orange", 1.5);
        Vegtable beet1 = factory.getVegetable(VegetableType.BEET, "Red", 2);
        Vegtable beet2 = factory.getVegetable(VegetableType.BEET, "Blue", 1);
      System.out.println(car1.isRipe());
      System.out.println(car2.isRipe());
      System.out.println(beet1.isRipe());
      System.out.println(beet2.isRipe());
      
    }
}
    
